var colors = ['cyan', 'blueviolet'];
var color_index = 0;

// This code is only executed on your personnal profile page.
// This only modify some front elements to change page appearance.

(function () {
	// Change background image by a personalized gif.
	// Timeout is set because page is asynchronously generated.
	setTimeout(function(){
		var huitsix = document.getElementsByClassName("container-inner-item")[0];
		var image = "url('" + chrome.extension.getURL("img/beer.gif") +"');" ;
		if (typeof huitsix == "undefined" || typeof image == "undefined") { return }
		huitsix.style = "background-image: " + image
		huitsix.style.backgroundPosition = "top"

		// Because of changes done to background image, font-color had to be modified on some elements
		var elems = [];
		elems.push(document.getElementsByClassName("visible-xs")[1])
		elems.push(document.getElementsByClassName("name")[0])
		elems.push(document.getElementsByClassName("login")[0])
		elems.forEach(function(elem) {
			elem.style.color = "#2c2c34f0";
		});
	}, 1000);

	// Change some elements color to a custom one
	setTimeout(function(){
		var color = colors[color_index];
		var coalition = document.getElementsByClassName("coalition-span");
		var logtime = document.getElementsByClassName("highcharts-series");
		var progress = document.getElementsByClassName("progress-bar")[2];
		var bh_status = document.querySelector('.status-bh');

		if (typeof coalition == "undefined" || typeof logtime == "undefined" || typeof progress == "undefined" || typeof bh_status == "undefined") { return }

		var i = 1;
		while (i < coalition.length){
			coalition[i].style.color = color;
			i += 1;
		}
		logtime[0].childNodes[0].style.fill = color;
		progress.style.background = color;
		bh_status.style.background = color;
	}, 1000);

}());
