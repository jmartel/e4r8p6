// This code is executed on any profile, to modify some front-end element if user is
// in a given and hard stored list of users.
// Logins hashs and stored in elite list, declared just below.

// List of logins, hasehd, that are considered as "in the club"
elite = [ 1016079762, -1495187737 , 824195583 , -200385724 , 481583924 , -1142934677 ]

// Function is used to hash login, and check if user is in the previous list.
String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) { return hash; }
    for (var i = 0; i < this.length; i++) {
        var char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash;
    }
    return hash;
}

// Get user login on the current page
var login_node = document.getElementsByClassName("login")[0];

if (typeof login_node != "undefined") {
		login = login_node.attributes["data-login"].value;
		hashed_login = login.hashCode();

	// If current user is ine the elite list it modify it's title and it's availability status
	if (elite.includes(hashed_login)) {
		// Modify it's title
		login_node.innerText = "clochard " + login;

		// If user is unavailable it modify it poste.
		poste_status_node = document.getElementsByClassName("user-poste-status")[0]
		poste_infos_node = document.getElementsByClassName("user-poste-infos")[0]
		if (poste_status_node.innerText == "Unavailable") {
			poste_status_node.innerText = "Available"
			poste_infos_node.innerText = "e4r8p6"
			poste_status_node.style.color = "rgb(83, 210, 122)"
			poste_infos_node.style.color = "rgb(83, 210, 122)"
		}
	}
}
