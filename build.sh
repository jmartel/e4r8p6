#!/bin/bash

n=/dev/null

type npm &>$n
if [ $? -ne 0 ] ; then please install npm ; exit 1 ; fi

type web-ext &>$n
if [ $? -eq 0 ] ; then
	bin="web-ext"
else
	if [ ! -f srcs/node_modules/web-ext/bin/web-ext ] ; then
		echo Istalling web-ext module
		cd srcs
		npm install web-ext &>$n && echo Installed web-ext
		if [ $? -ne 0 ] ; then echo Cannot install web-ext ; exit 1 ; fi
		cd ..
	fi
	bin="./srcs/node_modules/web-ext/bin/web-ext"
fi

$bin --version
